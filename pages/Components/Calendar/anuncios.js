import ImgPublic from '../../../public/Calendar/public.svg'
import Image from 'next/image'

export default function Anuncios() {

  return (
    <div className='bg-[#F7F7F7]'>
      <div className='grid grid-cols-12 ml-[25px] mr-[25px]'>
        <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between mt-[45px] mb-[25px]'>
            <div className='text-[14px] md:text-[32px] lg:text-[14px] text-[#000000] font-semibold'>
              May 7, 2022
            </div>
            <button className='text-[14px] md:text-[32px] lg:text-[14px] text-[#643DCE] font-normal'>
            
              View All
          
          </button>
        </div >
        <div className='col-span-12 md:col-span-12 lg:col-span-12 border-[2px] border-[#D9D9D9] rounded-[20px] pl-[20px] pt-[25px] pb-[20px] pr-[20px]'>
          <div className="flex flex-row">
            <div className="w-[48px] h-[48px] rounded-full bg-[#643DCE]">

            </div>
            <div className="pl-[10px]">

              <div className='text-[17px] font-semibold'>official_distributor</div>
              <p>Feb 2, 2022 11:20 P.M.</p>

            </div>

          </div>
          <div className='mt-[15px]'>
            <Image
              src={ImgPublic}
              layout='fixed'
              alt='ImgPublic'
              width={280}
              height={280}
              
            />
          </div>
          <div>
            #Italia tiene sus brillantes representantes en el pabellón europeo ¡Visítanos en la #Expocruz! 💚
          </div>

        </div>
        <div className='col-span-12 md:col-span-12 lg:col-span-12 mt-[15px]'> 

            <button className='w-full h-[48px] bg-[#582BE7] text-[#FFFFFF] font-semibold text-[18px] rounded-[25px] justify-center hover:bg-[#fff] hover:border-[#582BE7] hover:border-[2px] hover:text-[#582BE7]'>
            
            Edit post
            
            </button>
              
        </div>

    </div>
    </div>
  )
}
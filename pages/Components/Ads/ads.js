import Layout from "../../Layout/layout"

export default function Ads() {

  return (
    <div className='mt-[20px] ml-[20px] mr-[20px]'>

        <div className='grid grid-cols-12'>
            <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                <div className='text-[24px] md:text-[32px] lg:text-[32px] text-[#000000] '>Ads</div>
            </div>
        </div >
    </div>
  )
}

Ads.getLayout = function getLayout(page){
  return (
    <Layout>{page}</Layout>
  )
}

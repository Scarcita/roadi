import '../styles/globals.css'
import Router from 'next/router';
import  NProgress  from 'nprogress';
import 'nprogress/nprogress.css'
import { SessionProvider } from 'next-auth/react';


Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

function MyApp({ Component, pageProps: {session, ...pageProps} }) {

  const getLayout = Component.getLayout || ((page) => page )
  
  return (
    <SessionProvider session={session}>
      
      {getLayout(<Component {...pageProps} />)}
      
      
    </SessionProvider>
  );
}
export default MyApp

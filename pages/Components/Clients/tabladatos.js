import React from 'react';


import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'
import Image from 'next/image'



import { useEffect, useState } from 'react'

const Table = () => {

    const [showDots, setShowDots] = useState(true);
    const [total, setTotal] = useState(0);
    const [data, setData] = useState([]);

    useEffect(() => {
        fetch('https://slogan.com.bo/vulcano/orders/all/all/1')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    setData(data.data)

                } else {
                    console.error(data.error)
                }
            })
            .then(setShowDots(false))

    }, [])

    return (
        showDots ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                    <Spinner color="#bebebe" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <>
                <div className=' rounded-[22.35px] pt-6 h-full w-full overflow-auto'>
                    <TablaProductos data={data} />
                </div>
            </>
    )
}


const TablaProductos = (props) => {
    const { data } = props;
    console.log("assssssssssssssssssss");
    console.log(data);
    console.log(props);
    const map1 = data.map(row => console.log(row));

    const splitData = (obj) => {
        data.forEach(element => {
            let date = element.created.split("T");
            element.created = date[0];
        });
        data = obj;
    };
    splitData(data);

    return (

        <div className="'lg:w-full lg:h-[120px] bg-[#FFFF] grid grid-cols-12'">
            <table className="col-span-12 md:col-span-12 lg:col-span-12">
                <thead>
                    <tr className='text-black font-bold border-b-2 border-[#E4E7EB]'>
                        <th>Date</th>
                        <th>Client</th>
                        <th>Social Network</th>
                        <th>Post</th>
                        <th> Duaration</th>
                        <th>Amount</th>
                    </tr>
                </thead>

                <tbody>

                    {data.map(row =>

                        <tr key={row.id} >
                            <td className='h-8 w-16 '>
                                {row.created}
                            </td>
                            <td  className='h-8 w-16 font-bold'>
                                {row.contact_name}

                            </td> 
                            <td  className='h-8 w-16 text-center'>
                                <Image
                                    src={row.car.cars_models_version.cars_model.catalogues_record.additional_info}
                                    alt="brand"
                                    layout="fixed"
                                    width={40}
                                    height={40}
                                />

                            </td>
                            <td  className='h-8 w-16'>
                                {row.car.cars_models_version.cars_model.name}

                            </td>


                            <td  className='h-8 w-16'>
                                {row.created}


                            </td>
                            <td  className='h-8 w-16'>
                                {row.car.plate}

                            </td>


                        </tr>
                    )}


                </tbody>
            </table>
        </div>
    );
};





export default Table;
import Layout from "../../Layout/layout"
import ImgPublic from '../../../public/Calendar/public.svg'
import Image from 'next/image'
import Anuncios from "./anuncios"
import GraficCalendar from "./graficCalendar"



export default function Calendar() {

  return (
      <div className='grid grid-cols-12'>
        <div className='col-span-12 md:col-span-12 lg:col-span-8 ml-[30px] mr-[30px] mt-[45px]'>
          <div className='grid grid-cols-12'>
              <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                  <div className='text-[24px] md:text-[32px] lg:text-[37px] text-[#000000] font-bold'>Calendar</div>
                  <button className='flex flex-row w-[140px] h-[38px] md:w-[200px] md:h-[48px] lg:w-[240px] lg:h-[48px] bg-[#582BE7] text-[#FFFFFF] font-semibold text-[18px] rounded-[25px] hover:bg-[#fff] hover:border-[#582BE7] hover:border-[2px] hover:text-[#582BE7] justify-center'>
                  
                    Create post
                
                </button>
                  
              </div>
              
          </div >
          <div className="mt-[24px] text-[18px] font-semibold">
            Status
          </div>
            <div className='grid grid-cols-12 gap-3 mt-[24px]'>
              
                <div className='col-span-12 md:col-span-6 lg:col-span-6 h-[120px] bg-[#F3F3F3] rounded-[17px] justify-center text-center pt-[30px]'>
                  <div className="text-[12px] font-medium text-[#000000] ">Active clients</div>
                </div>
                  
                  
                <div className='col-span-6 md:col-span-3 lg:col-span-3 h-[120px] bg-[#F3F3F3] rounded-[17px] justify-center text-center pt-[20px]'>

                  <div className="text-[32px] font-semibold text-[#000000] text-center" >10</div>
                  <div className="text-[12px] font-medium text-[#000000] text-center">Scheduled for today</div>

                </div>

                <div className='col-span-6 md:col-span-3 lg:col-span-3 h-[120px] bg-[#F3F3F3] rounded-[17px] justify-center text-center pt-[20px]'>

                  <div className="text-[32px] font-semibold text-[#000000] " >27</div>
                  <div className="text-[12px] font-medium text-[#000000] ">Posted this week</div>
                  
                </div>
              
            </div >

            <div className='grid grid-cols-12 mt-[35px]'>
            
              <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                <GraficCalendar/>

              </div>

          </div>
        </div>
        <div className='col-span-12 md:col-span-12 lg:col-span-4'>
          <div className='grid grid-cols-12'>
            <div className='col-span-12 md:col-span-12 lg:col-span-12'>
              <Anuncios/>
            </div>
          </div>

        </div>
      </div>
  )
}

Calendar.getLayout = function getLayout(page){
  return (
    <Layout>{page}</Layout>
  )
}


import Layout from '../../Layout/layout'
import Image from 'next/image'
import BarData from './barChart'
import Public from './public'

 /////// IMAGENES ////////////////////
 import ImgFacebook from '../../../public/Dashhoard/facebook.svg'
 import ImgGmail from '../../../public/Dashhoard/gmail.svg'
 import ImgIg from '../../../public/Dashhoard/instagram.svg'
 import ImgMessenger from '../../../public/Dashhoard/messenger.svg'
 import ImgTelegram from '../../../public/Dashhoard/telegram.svg'
 import ImgTiktok from '../../../public/Dashhoard/tikTok.svg'
 import ImgTwitter from '../../../public/Dashhoard/twitter.svg'
 import ImgWhatsapp from '../../../public/Dashhoard/whatsapp.svg'
 import ImgYoutube from '../../../public/Dashhoard/youtube.svg'

export default function Dashoard() {

  return (
    <div className='grid grid-cols-12'>
        <div className='col-span-12 md:col-span-12 lg:col-span-8 ml-[30px] mr-[30px] mt-[45px]'>
          <div className='grid grid-cols-12'>
              <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                  <div className='text-[24px] md:text-[32px] lg:text-[37px] text-[#000000] font-bold'>
                    Calendar
                  </div> 
              </div>
              
          </div >
          <div className="mt-[24px] text-[18px] font-semibold">
            Status
          </div>
          <div className="mt-[20px] grid grid-cols-12 gap-4">
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[180deg, #FCA759 -3.12%, #E82D56 17.47%, #A22DB4 77.07%, #643DCE 105.24%] rounded-[16px] shadow-md  justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <text className="text-[52px] font-semibold text-[#3682F7]">112</text>
                          
                  </div>
                  <div className="text-[10px] text-[#A5A5A5]">POST</div>
                </div>
                <div className=''>
                  <Image
                    src={ImgFacebook}
                    layout='fixed'
                    alt='ImgFacebook'
                    width={48}
                    height={48}
                    
                  />
                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#FFFF] rounded-[16px] shadow-md  justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <text className="text-[52px] font-semibold text-[#3682F7]">112</text>
                          
                  </div>
                  <div className="text-[10px] text-[#A5A5A5]">POST</div>
                </div>
                <div className=''>
                  <Image
                    src={ImgGmail}
                    layout='fixed'
                    alt='ImgGmail'
                    width={48}
                    height={48}
                    
                  />
                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#FFFF] rounded-[16px] shadow-md  justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <text className="text-[52px] font-semibold text-[#3682F7]">112</text>
                          
                  </div>
                  <div className="text-[10px] text-[#A5A5A5]">POST</div>
                </div>
                <div className=''>
                  <Image
                    src={ImgIg}
                    layout='fixed'
                    alt='ImgIg'
                    width={48}
                    height={48}
                    
                  />
                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#FFFF] rounded-[16px] shadow-md  justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <text className="text-[52px] font-semibold text-[#3682F7]">112</text>
                          
                  </div>
                  <div className="text-[10px] text-[#A5A5A5]">POST</div>
                </div>
                <div className=''>
                  <Image
                    src={ImgMessenger}
                    layout='fixed'
                    alt='ImgMessenger'
                    width={48}
                    height={48}
                    
                  />
                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#FFFF] rounded-[16px] shadow-md  justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <text className="text-[52px] font-semibold text-[#3682F7]">112</text>
                          
                  </div>
                  <div className="text-[10px] text-[#A5A5A5]">POST</div>
                </div>
                <div className=''>
                  <Image
                    src={ImgTelegram}
                    layout='fixed'
                    alt='ImgTelegram'
                    width={48}
                    height={48}
                    
                  />
                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#FFFF] rounded-[16px] shadow-md  justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <text className="text-[52px] font-semibold text-[#3682F7]">112</text>
                          
                  </div>
                  <div className="text-[10px] text-[#A5A5A5]">POST</div>
                </div>
                <div className=''>
                  <Image
                    src={ImgTwitter}
                    layout='fixed'
                    alt='ImgTwitter'
                    width={48}
                    height={48}
                    
                  />
                
                </div>
            </div>
          </div>

          <div className="mt-[24px] text-[18px] font-semibold">
          Accounts status
          </div>
          <div className="mt-[20px] grid grid-cols-12 gap-4">
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#FFFF] rounded-[16px] shadow-md  justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <text className="text-[32px] font-semibold text-[#000000]">112</text>
                          
                  </div>
                  <div className="text-[10px] text-[#000000]">ACTIVE PLANS</div>
                </div>
                <div className='w-[48px] h-[48px] bg-[#3682F7] rounded-[16px]'>
                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#FFFF] rounded-[16px] shadow-md  justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <text className="text-[32px] font-semibold text-[#000000]">11,120.80</text>
                          
                  </div>
                  <div className="text-[10px] text-[#000000]">PLANS TOTAL</div>
                </div>
                <div className='w-[48px] h-[48px] bg-[#3682F7] rounded-[16px]'>
                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#FFFF] rounded-[16px] shadow-md  justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <text className="text-[32px] font-semibold text-[#000000]">11,120.80</text>
                          
                  </div>
                  <div className="text-[10px] text-[#000000]">ADS TOTAL</div>
                </div>
                <div className='w-[48px] h-[48px] bg-[#3682F7] rounded-[16px]'>
                
                </div>
            </div>
          </div>

          <div className="mt-[24px] text-[18px] font-semibold">
          Ads overview
          </div>

          <div className='grid grid-cols-12 mt-[15px]'>
            <div className="col-span-12 md:col-span-12 lg:col-span-12 h-[190px] md:h-[188px] lg:h-[188px] bg-[#FFFFFF] rounded-[24px] pt-[10px] pr-[5px]">
                <BarData/>

            </div>
          </div> 

        </div>
        <div className='col-span-12 md:col-span-12 lg:col-span-4'>
          <div className='grid grid-cols-12'>
            <div className='col-span-12 md:col-span-12 lg:col-span-12'>
              <Public/>
            </div>
          </div>

        </div>
      
      </div>
  )
}

Dashoard.getLayout = function getLayout(page){
  return (
    <Layout>{page}</Layout>
  )
}

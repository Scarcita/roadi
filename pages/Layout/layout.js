import Link from 'next/link';
import Image from 'next/image'
import { useRouter } from 'next/router';

import { useEffect, useRef } from 'react';


const Layout = ({ children }) => {

  const router = useRouter();

  const mobileMenu = useRef();

  const menuItems = [
    
    {
      href: '/Components/Dashoard/dashoard',
      title: 'Dashoard',
      type: 'home'
    },
    {
      href: '/Components/Calendar/calendar',
      title: 'Calendar',
      type: 'citas'
    },
    {
      href: '/Components/Ads/ads',
      title: 'ads',
      type: 'citas'
    },
    {
      href: '/Components/Clients/clients',
      title: 'clients',
      type: 'asignar'
    },
    {
      href: '/Components/Plans/plans',
      title: 'plans',
      type: 'asignar'
    },
    

  ];

  return (
    <div className="min-h-screen flex flex-col bg-[#F6F6FA]">

      <div className="h-screen grid grid-cols-12 w-full bg-[#FFFFFF]">
        <aside className='hidden md:block md:col-span-2 bg-[#0000000] w-full  md:h-screen lg:flex lg:flex-col divide-x '>
          <div className='flex flex-row justify-center items-center h-1/6 relative top-0 bg-[#FFFFFF]'>
            <div className='font-semibold text-[24px]'>ROADIE</div>
          </div>
          <div className='w-full h-3/4 overflow-auto'>
            
            <ul>
              {menuItems.map(({ href, title, type }) => (

                <li key={title} >
                  <Link href={href} >
                    <a
                      className={
                        router.asPath === href ?
                          'w-5/6 flex flex-row m-auto justify-center items-center rounded-lg  h-14 my-2 text-[18px] font-semibold text-[#643DCE] cursor-pointer'
                          : 'w-5/6 flex flex-row m-auto justify-center rounded-lg items-center bg-[#FFFFFF] h-14 my-2 text-[18px] font-semibold text-[#A7A7B7] hover:text-[#643DCE] cursor-pointer'}
                    >
                      <div className='w-2/6 flex flex-row justify-end items-center'>
                        {
                          title == 'Dashoard' ?
                          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                          <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 6a7.5 7.5 0 107.5 7.5h-7.5V6z" />
                          <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 10.5H21A7.5 7.5 0 0013.5 3v7.5z" />
                          </svg>
                            : null
                        }
                        {
                          title == 'Calendar' ?
                          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                          <path stroke-linecap="round" stroke-linejoin="round" d="M6.75 3v2.25M17.25 3v2.25M3 18.75V7.5a2.25 2.25 0 012.25-2.25h13.5A2.25 2.25 0 0121 7.5v11.25m-18 0A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75m-18 0v-7.5A2.25 2.25 0 015.25 9h13.5A2.25 2.25 0 0121 11.25v7.5m-9-6h.008v.008H12v-.008zM12 15h.008v.008H12V15zm0 2.25h.008v.008H12v-.008zM9.75 15h.008v.008H9.75V15zm0 2.25h.008v.008H9.75v-.008zM7.5 15h.008v.008H7.5V15zm0 2.25h.008v.008H7.5v-.008zm6.75-4.5h.008v.008h-.008v-.008zm0 2.25h.008v.008h-.008V15zm0 2.25h.008v.008h-.008v-.008zm2.25-4.5h.008v.008H16.5v-.008zm0 2.25h.008v.008H16.5V15z" />
                          </svg>
                            : null
                        }
                        {
                          title == 'ads' ?
                          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                          <path stroke-linecap="round" stroke-linejoin="round" d="M10.34 15.84c-.688-.06-1.386-.09-2.09-.09H7.5a4.5 4.5 0 110-9h.75c.704 0 1.402-.03 2.09-.09m0 9.18c.253.962.584 1.892.985 2.783.247.55.06 1.21-.463 1.511l-.657.38c-.551.318-1.26.117-1.527-.461a20.845 20.845 0 01-1.44-4.282m3.102.069a18.03 18.03 0 01-.59-4.59c0-1.586.205-3.124.59-4.59m0 9.18a23.848 23.848 0 018.835 2.535M10.34 6.66a23.847 23.847 0 008.835-2.535m0 0A23.74 23.74 0 0018.795 3m.38 1.125a23.91 23.91 0 011.014 5.395m-1.014 8.855c-.118.38-.245.754-.38 1.125m.38-1.125a23.91 23.91 0 001.014-5.395m0-3.46c.495.413.811 1.035.811 1.73 0 .695-.316 1.317-.811 1.73m0-3.46a24.347 24.347 0 010 3.46" />
                        </svg>

                            : null
                        }

                        {
                          title == 'clients' ?
                          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                          <path stroke-linecap="round" stroke-linejoin="round" d="M18 18.72a9.094 9.094 0 003.741-.479 3 3 0 00-4.682-2.72m.94 3.198l.001.031c0 .225-.012.447-.037.666A11.944 11.944 0 0112 21c-2.17 0-4.207-.576-5.963-1.584A6.062 6.062 0 016 18.719m12 0a5.971 5.971 0 00-.941-3.197m0 0A5.995 5.995 0 0012 12.75a5.995 5.995 0 00-5.058 2.772m0 0a3 3 0 00-4.681 2.72 8.986 8.986 0 003.74.477m.94-3.197a5.971 5.971 0 00-.94 3.197M15 6.75a3 3 0 11-6 0 3 3 0 016 0zm6 3a2.25 2.25 0 11-4.5 0 2.25 2.25 0 014.5 0zm-13.5 0a2.25 2.25 0 11-4.5 0 2.25 2.25 0 014.5 0z" />
                          </svg>
                        
                            : null
                        }

                        {
                          title == 'plans' ?
                          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                          <path stroke-linecap="round" stroke-linejoin="round" d="M20.25 8.511c.884.284 1.5 1.128 1.5 2.097v4.286c0 1.136-.847 2.1-1.98 2.193-.34.027-.68.052-1.02.072v3.091l-3-3c-1.354 0-2.694-.055-4.02-.163a2.115 2.115 0 01-.825-.242m9.345-8.334a2.126 2.126 0 00-.476-.095 48.64 48.64 0 00-8.048 0c-1.131.094-1.976 1.057-1.976 2.192v4.286c0 .837.46 1.58 1.155 1.951m9.345-8.334V6.637c0-1.621-1.152-3.026-2.76-3.235A48.455 48.455 0 0011.25 3c-2.115 0-4.198.137-6.24.402-1.608.209-2.76 1.614-2.76 3.235v6.226c0 1.621 1.152 3.026 2.76 3.235.577.075 1.157.14 1.74.194V21l4.155-4.155" />
                          </svg>
                        
                            : null
                        }

                      </div>
                      <div className='w-4/6 flex flex-row justify-start items-center mx-2'>
                        <p>
                          {title}
                        </p>
                      </div>
                    </a>
                  </Link>
                </li>
              ))}
            </ul>
          </div>

          <div className='grid grid-cols-12'>
            <div className='col-span-12 md:col-span-12 lg:col-span-12 text-center'>
                <div className='bg-[#E4E7EB] w-[77px] h-[77px] rounded-full justify-center'></div>

                <div className=' text-center mx-2'>
        
                  Want Admin

                </div>

                <div className='text-center mx-2'>
             
                  @wantdigitalagency

                </div>
          
            </div>
            <div className='col-span-12 md:col-span-12 lg:col-span-12 text-center'>
              <button className='flex flex-row w-[140px] h-[56px] bg-[#582BE7] text-[#fff] font-semibold text-[18px] rounded-[28px] justify-center hover:bg-[#fff] hover:border-[#582BE7] hover:border-[2px] hover:text-[#582BE7] pt-[15px]'>
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" className="w-6 h-6 mr-1">
                <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9"/>
                </svg>

                  Sing out
              
              </button>
            </div>
            </div>
        </aside>

{/* ////////////////////////////////////////////////////////////////////////////////////////////////// */}

        <div className='md:hidden col-span-12 flex justify-between p-2 pt-4 pb-4'>
          {/* <Image
            src={ImgLogo}
            layout='fixed'
            alt='ImgLogo'
          /> */}
          <div className="flex items-center">
            <button className="outline-none mobile-menu-button">
              <svg className=" w-6 h-6 text-gray-500 hover:text-[#643DCE] "
                // x-show="!showMenu"
                fill="none"
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path d="M4 6h16M4 12h16M4 18h16"></path>
              </svg>
            </button>
          </div>
        </div>
        <div class="hidden md:hidden col-span-12 mobile-menu" ref={mobileMenu} >

          <ul>
            {menuItems.map(({ href, title, type }) => (
              <li key={title} className={router.asPath === href && 'bg-[#3682F7] text-[#FFFF] hover:bg-[#643DCE] hover:text-[#FFFFFF]'}>
                <Link href={href} >
                  <a className={'w-full mt-2 mb-2 ml-4 text-[12px] text-right'}>
                    {title}
                  </a>
                </Link>
              </li>
            ))}
          </ul>

        </div>

        <main className="col-span-12 md:col-span-10 ">{children}</main>
      </div>

    </div>
  );
}

export default Layout;
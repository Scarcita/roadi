import Layout from "../../Layout/layout"
//import Table from "./tabladatos";
//import Graficos from "./graficos";

export default function Clients() {

  return (
    

      <div className="w-screen h-screen bg-blue-500">
        <div className="bg-red-400 h-2/5 w-full">
          <div className="h-20 w-full flex flex-row bg-lime-100 ">
            <div className="w-1/2 h-16 ml-8 flex  items-center bg-[#fe5]">
              <p className="text-4xl text-blck font-bold">Ads</p>
            </div>
            <div className="w-1/2 h-full flex justify-end bg-lime-200">
              <button className="w-60 h-16 mt-4 flex justify-center items-center text-center text-white text-xl font-bold bg-purple-700 rounded-full py-3 px-6...">
                Create Ad
              </button>
            </div>

          </div>

          <div className="w-full h-96 bg-yellow-800 flex flex-row">
            <div className="w-1/2 h-3/5  bg-[#fce]">
              <div className="ml-8">
                <p className="text-lg text-black font-bold">
                  Month overview
                </p>
              </div>
              <div className="ml-8 mr-8">
                {/* <Graficos /> */}

              </div>
            </div>

            <div className="w-1/2 h-3/5 bg-[#f5c]">
              <div className="ml-8">
                <p className="text-lg text-black font-bold">
                  Month overview
                </p>
              </div>
              <div className=" w-full h-3/5 flex flex-row">
                <div className="w-1/2 bg-[#fff]">
                  <div className="w-full flex flex-row justify-center items-center bg-[#ef32]">
                    <div className="m-2">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-6 h-6">
                        <path d="M7.493 18.75c-.425 0-.82-.236-.975-.632A7.48 7.48 0 016 15.375c0-1.75.599-3.358 1.602-4.634.151-.192.373-.309.6-.397.473-.183.89-.514 1.212-.924a9.042 9.042 0 012.861-2.4c.723-.384 1.35-.956 1.653-1.715a4.498 4.498 0 00.322-1.672V3a.75.75 0 01.75-.75 2.25 2.25 0 012.25 2.25c0 1.152-.26 2.243-.723 3.218-.266.558.107 1.282.725 1.282h3.126c1.026 0 1.945.694 2.054 1.715.045.422.068.85.068 1.285a11.95 11.95 0 01-2.649 7.521c-.388.482-.987.729-1.605.729H14.23c-.483 0-.964-.078-1.423-.23l-3.114-1.04a4.501 4.501 0 00-1.423-.23h-.777zM2.331 10.977a11.969 11.969 0 00-.831 4.398 12 12 0 00.52 3.507c.26.85 1.084 1.368 1.973 1.368H4.9c.445 0 .72-.498.523-.898a8.963 8.963 0 01-.924-3.977c0-1.708.476-3.305 1.302-4.666.245-.403-.028-.959-.5-.959H4.25c-.832 0-1.612.453-1.918 1.227z" />
                      </svg>

                    </div>
                    <div>
                      <div>
                        <p className="text-center text-3xl text-black font-bold">12,787,70</p>
                      </div>
                      <div>
                        <p className="text-gray-400	">TOTAL ASD</p>
                      </div>
                    </div>
                    <div className="mb-6 m-2">
                      <p className="text-black">
                        USD
                      </p>
                    </div>

                  </div>
                  <div className="w-full flex flex-row justify-center items-center bg-[#0cf] mt-4">
                    <div className="m-2">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-6 h-6">
                        <path d="M7.493 18.75c-.425 0-.82-.236-.975-.632A7.48 7.48 0 016 15.375c0-1.75.599-3.358 1.602-4.634.151-.192.373-.309.6-.397.473-.183.89-.514 1.212-.924a9.042 9.042 0 012.861-2.4c.723-.384 1.35-.956 1.653-1.715a4.498 4.498 0 00.322-1.672V3a.75.75 0 01.75-.75 2.25 2.25 0 012.25 2.25c0 1.152-.26 2.243-.723 3.218-.266.558.107 1.282.725 1.282h3.126c1.026 0 1.945.694 2.054 1.715.045.422.068.85.068 1.285a11.95 11.95 0 01-2.649 7.521c-.388.482-.987.729-1.605.729H14.23c-.483 0-.964-.078-1.423-.23l-3.114-1.04a4.501 4.501 0 00-1.423-.23h-.777zM2.331 10.977a11.969 11.969 0 00-.831 4.398 12 12 0 00.52 3.507c.26.85 1.084 1.368 1.973 1.368H4.9c.445 0 .72-.498.523-.898a8.963 8.963 0 01-.924-3.977c0-1.708.476-3.305 1.302-4.666.245-.403-.028-.959-.5-.959H4.25c-.832 0-1.612.453-1.918 1.227z" />
                      </svg>

                    </div>
                    <div>
                      <div>
                        <p className="text-center text-3xl text-black font-bold">12,787,70</p>
                      </div>
                      <div>
                        <p className="text-gray-400	">TOTAL ASD</p>
                      </div>
                    </div>
                    <div className="mb-6 m-2">
                      <p className="text-black">
                        USD
                      </p>
                    </div>

                  </div>
                </div>



                <div className="w-1/2 bg-[#0ff]">
                  <div className="w-full flex flex-row justify-center items-center bg-[#ef32]">
                    <div className="m-2">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-6 h-6">
                        <path d="M7.493 18.75c-.425 0-.82-.236-.975-.632A7.48 7.48 0 016 15.375c0-1.75.599-3.358 1.602-4.634.151-.192.373-.309.6-.397.473-.183.89-.514 1.212-.924a9.042 9.042 0 012.861-2.4c.723-.384 1.35-.956 1.653-1.715a4.498 4.498 0 00.322-1.672V3a.75.75 0 01.75-.75 2.25 2.25 0 012.25 2.25c0 1.152-.26 2.243-.723 3.218-.266.558.107 1.282.725 1.282h3.126c1.026 0 1.945.694 2.054 1.715.045.422.068.85.068 1.285a11.95 11.95 0 01-2.649 7.521c-.388.482-.987.729-1.605.729H14.23c-.483 0-.964-.078-1.423-.23l-3.114-1.04a4.501 4.501 0 00-1.423-.23h-.777zM2.331 10.977a11.969 11.969 0 00-.831 4.398 12 12 0 00.52 3.507c.26.85 1.084 1.368 1.973 1.368H4.9c.445 0 .72-.498.523-.898a8.963 8.963 0 01-.924-3.977c0-1.708.476-3.305 1.302-4.666.245-.403-.028-.959-.5-.959H4.25c-.832 0-1.612.453-1.918 1.227z" />
                      </svg>

                    </div>
                    <div>
                      <div>
                        <p className="text-center text-3xl text-black font-bold">12,787,70</p>
                      </div>
                      <div>
                        <p className="text-gray-400	">TOTAL ASD</p>
                      </div>
                    </div>
                    <div className="mb-6 m-2">
                      <p className="text-black">
                        USD
                      </p>
                    </div>

                  </div>
                  <div className="w-full flex flex-row justify-center items-center bg-[#0cf] mt-4">
                    <div className="m-2">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-6 h-6">
                        <path d="M7.493 18.75c-.425 0-.82-.236-.975-.632A7.48 7.48 0 016 15.375c0-1.75.599-3.358 1.602-4.634.151-.192.373-.309.6-.397.473-.183.89-.514 1.212-.924a9.042 9.042 0 012.861-2.4c.723-.384 1.35-.956 1.653-1.715a4.498 4.498 0 00.322-1.672V3a.75.75 0 01.75-.75 2.25 2.25 0 012.25 2.25c0 1.152-.26 2.243-.723 3.218-.266.558.107 1.282.725 1.282h3.126c1.026 0 1.945.694 2.054 1.715.045.422.068.85.068 1.285a11.95 11.95 0 01-2.649 7.521c-.388.482-.987.729-1.605.729H14.23c-.483 0-.964-.078-1.423-.23l-3.114-1.04a4.501 4.501 0 00-1.423-.23h-.777zM2.331 10.977a11.969 11.969 0 00-.831 4.398 12 12 0 00.52 3.507c.26.85 1.084 1.368 1.973 1.368H4.9c.445 0 .72-.498.523-.898a8.963 8.963 0 01-.924-3.977c0-1.708.476-3.305 1.302-4.666.245-.403-.028-.959-.5-.959H4.25c-.832 0-1.612.453-1.918 1.227z" />
                      </svg>

                    </div>
                    <div>
                      <div>
                        <p className="text-center text-3xl text-black font-bold">12,787,70</p>
                      </div>
                      <div>
                        <p className="text-gray-400	">TOTAL ASD</p>
                      </div>
                    </div>
                    <div className="mb-6 m-2">
                      <p className="text-black">
                        USD
                      </p>
                    </div>

                  </div>
                </div>

              </div>
            </div>
          </div>


        </div>

        <div className="h-3/5 bg-green-400  ml-8">
          {/* <Table /> */}
        </div>
      </div>
    )
  }
    
  

Clients.getLayout = function getLayout(page) {
  return (
    <Layout>{page}</Layout>
  )
}

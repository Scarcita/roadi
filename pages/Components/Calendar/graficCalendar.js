
import React from 'react';
import Calendar from 'react-calendar';

export default class GraficCalendar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(2019, 4, 17),
      //月のデータ
      month_days: {
        20190501: { is_holiday: true },
        20190502: { is_holiday: true },
        20190503: { is_holiday: true },
        20190506: { is_holiday: true },
        20190514: { text: 'Dia de la madre' },
        20190517: { text: 'Dia del padre' }
      }
    };
    this.getTileClass = this.getTileClass.bind(this);
    this.getTileContent = this.getTileContent.bind(this);
  }

  // state の日付と同じ表記に変換
  getFormatDate(date) {
    return `${date.getFullYear()}${('0' + (date.getMonth() + 1)).slice(-2)}${('0' + date.getDate()).slice(-2)}`;
  }

  //日付のクラスを付与 (祝日用)
  getTileClass({ date, view }) {
    // 月表示のときのみ
    if (view !== 'month') {
      return '';
    }
    const day = this.getFormatDate(date);
    return (this.state.month_days[day] && this.state.month_days[day].is_holiday) ?
      'holiday' : '';
  }

  //日付の内容を出力
  getTileContent({ date, view }) {
    // 月表示のときのみ
    if (view !== 'month') {
      return null;
    }
    const day = this.getFormatDate(date);
    return (
      <p>
        <br />
        {(this.state.month_days[day] && this.state.month_days[day].text) ?
          this.state.month_days[day].text : ''
        }
      </p>
    );
  }

  render() {
    return (
      <div className='grid grid-cols-12'>
        <div className='col-span-12 md:col-span-12 lg:col-span-12'>
          <Calendar
            locale="bo-BO"
            value={this.state.date}
            tileClassName={this.getTileClass}
            tileContent={this.getTileContent}
          />
        </div>
      </div>

    );
  }
}
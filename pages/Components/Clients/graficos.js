// import React from 'react';
// import Spinner from 'react-activity/dist/Spinner';
// import 'react-activity/dist/Spinner.css';
// import { useEffect, useState } from 'react';
// import {
//   Chart as ChartJS,
//   CategoryScale,
//   LinearScale,
//   BarElement,
//   Title,
//   Tooltip,
//   Legend,
// } from 'chart.js';
// import { Bar } from 'react-chartjs-2';

// ChartJS.register(
//   CategoryScale,
//   LinearScale,
//   BarElement,
//   Title,
//   Tooltip,
//   Legend
// );

// export default function Graficos() {
//   const options = {
//     responsive: true,
//     plugins: {
//       legend: {
//         display: false,
//         position: "top",
//       },
//       title: {
//         display: false,
//         text: 'Chart.js Bar Chart',
//       },
//     },
//   };

//   const [showDots, setShowDots] = useState(true);

//   const [data, setData] = useState([]); ([]);
//   const [dataGraph, setDataGraph] = useState(null);

//   const getDatos = () => {

//     fetch('https://slogan.com.bo/vulcano/orders/yearlyGraph')
//       .then(response => response.json())
//       .then(data => {
//         if (data) {
//           console.log(data.data);
//           setData(data.data)
//           console.log(data.data.labels);

//           const labels = data.data.labels;

//           setDataGraph(
//             {
//               labels,
//               datasets: [
//                 {
//                   label: 'Dataset 1',
//                   data: data.data.data,
//                   backgroundColor: '#3682F7',
//                 },
//               ],
//             }
//           );
//         } else {
//           console.error(data.error)
//         }
//       })
//       .then(setShowDots(false))
//   }

//   useEffect(() => {
//     getDatos();
//   }, []);

//   const plugins = [{
//     afterDraw: chart => {
//       let ctx = chart.chart.ctx;
//       ctx.save();
//       let xAxis = chart.scales['x-axis-0'];
//       let yAxis = chart.scales['y-axis-0'];
//       xAxis.ticks.forEach((value, index) => {
//         let x = xAxis.getPixelForTick(index);
//         let image = new Image();
//         image.src = 'https://i.pinimg.com/originals/d0/b9/e4/d0b9e4b888a07e67f1b2d8f09fcfbe64.png', 
//         ctx.drawImage(image, x - 12, yAxis.bottom + 10);
//     });
//   ctx.restore();
// }}];

// return (
//   showDots ?
//     <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
//       <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
//     </div>
//     :
//     <>
    
//       <div >
        
        
//         {dataGraph != null ?
//           <Bar options={options} data={dataGraph} width={150} height={30} plugins={plugins} pb={60}/>
//            : <></>
//         }
//       </div>
//     </>
// )
// };